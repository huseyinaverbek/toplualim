﻿using Nop.Core.Domain.BulkOrder;
using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.BulkOrder
{
    public partial record TopluTalepModel : BaseNopEntityModel 
    {
        public int Quantity { get; set; }
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
        public int BulkOrderStatusId { get; set; }
        public BulkOrderStatus BulkOrderStatusEnum { get; set; }

        public DateTime BulkOrderCreatedOnUtc { get; set; }
    }
}
