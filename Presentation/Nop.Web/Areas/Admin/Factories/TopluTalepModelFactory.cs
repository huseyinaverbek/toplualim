﻿using Nop.Core;
using Nop.Core.Domain.BulkOrder;
using Nop.Services.BulkOrder;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Topics;
using Nop.Web.Areas.Admin.Models.BulkOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Factories
{
    public partial class TopluTalepModelFactory : ITopluTalepModelFactory
    {
        #region Fields

        private readonly IAclService _aclService;
        private readonly ILocalizationService _localizationService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly ITopluTalepService _talepService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IDateTimeHelper _dateTimeHelper;

        #endregion

        public TopluTalepModelFactory(IAclService aclService,
        ILocalizationService localizationService,
        IStoreContext storeContext,
        IStoreMappingService storeMappingService,
        ITopluTalepService talepService,
        IUrlRecordService urlRecordService,
        IDateTimeHelper dateTimeHelper)
        {
            _aclService = aclService;
            _localizationService = localizationService;
            _storeContext = storeContext;
            _storeMappingService = storeMappingService;
            _talepService = talepService;
            _urlRecordService = urlRecordService;
            _dateTimeHelper = dateTimeHelper;
        }



        #region Utilities

        /// <summary>
        /// Prepare the topic model
        /// </summary>
        /// <param name="talep">Topic</param>
        /// <returns>
        /// A task that represents the asynchronous operation
        /// The task result contains the opic model
        /// </returns>
        protected virtual async Task<TopluTalepModel> PrepareTalepModelAsync(TopluTalep talep)
        {
            if (talep == null)
                throw new ArgumentNullException(nameof(talep));

            var model = new TopluTalepModel
            {
                Id = talep.Id,
                Quantity = talep.Quantity,
                ProductId = talep.ProductId,
                CustomerId = talep.CustomerId,
                BulkOrderStatusId = talep.BulkOrderStatusId,
                BulkOrderCreatedOnUtc = await _dateTimeHelper.ConvertToUserTimeAsync(talep.BulkOrderCreatedOnUtc, DateTimeKind.Utc),
            };

            return model;
        }

        #endregion



        public virtual async Task<TopluTalepModel> PrepareTalepModelByIdAsync(int talepId, bool showHidden = false)
        {
            var talep = await _talepService.GetTalepByIdAsync(talepId);

            if (talep == null)
                return null;

            if (showHidden)
                return await PrepareTalepModelAsync(talep);

            return await PrepareTalepModelAsync(talep);
        }


        public virtual async Task<TopluTalepListModel> PrepareTopluTalepListModelAsync()
        {
            var taleplist = await _talepService.GetAllTalepsAsync();


            var model = new TopluTalepListModel
            {
                taleplist = await taleplist.SelectAwait(async talep =>
                {
                    var talepModel = new TopluTalepModel();
                    await PrepareTalepModelAsync(talep);
                    return talepModel;
                }).ToListAsync()
            };


            return model;
        }
    }
}
