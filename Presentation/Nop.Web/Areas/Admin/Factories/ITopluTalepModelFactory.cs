﻿using Nop.Web.Areas.Admin.Models.BulkOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Nop.Web.Areas.Admin.Factories
{
    public partial interface ITopluTalepModelFactory
    {
        /// <summary>
        /// Get the topic model by topic identifier
        /// </summary>
        /// <param name="topicId">Topic identifier</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>
        /// A task that represents the asynchronous operation
        /// The task result contains the opic model
        /// </returns>
        Task<TopluTalepModel> PrepareTalepModelByIdAsync(int talepId, bool showHidden = false);

        Task<TopluTalepListModel> PrepareTopluTalepListModelAsync();
    }
}
