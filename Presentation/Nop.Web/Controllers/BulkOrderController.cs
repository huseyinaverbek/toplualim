﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.BulkOrder;
using Nop.Services.BulkOrder;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Factories;
using Nop.Web.Models.BulkOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Controllers
{
    public partial class BulkOrderController : BasePublicController
    {

        #region Fields

        private readonly IAclService _aclService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly ITopluTalepModelFactory _toplutalepModelFactory;
        private readonly ITopluTeklifModelFactory _topluteklifModelFactory;
        private readonly ITopluTalepService _toplutalepService;
        private readonly ITopluTeklifService _topluteklifService;

        #endregion

        #region Ctor
        public BulkOrderController(IAclService aclService,
            ILocalizationService localizationService,
            IPermissionService permissionService,
            IStoreMappingService storeMappingService,
            ITopluTalepModelFactory toplutalepModelFactory,
            ITopluTeklifModelFactory topluTeklifModelFactory,
            ITopluTalepService topluTalepService,
            ITopluTeklifService topluTeklifService)
        {
            _aclService = aclService;
            _localizationService = localizationService;
            _permissionService = permissionService;
            _storeMappingService = storeMappingService;
            _toplutalepModelFactory = toplutalepModelFactory;
            _topluteklifModelFactory = topluTeklifModelFactory;
            _toplutalepService = topluTalepService;
            _topluteklifService = topluTeklifService;


        }
        #endregion

        [HttpPost, ActionName("AddTalep")]
        public virtual async Task<IActionResult> TopluTalepAdd(int toplutalepId, TopluTalepModel model)
        {
            var talep = new TopluTalep
            {
                ProductId = model.ProductId,
                CustomerId = model.CustomerId,
                Quantity = model.Quantity,
                BulkOrderCreatedOnUtc = model.BulkOrderCreatedOnUtc,
                BulkOrderStatusId = model.BulkOrderStatusId
            };

            await _toplutalepService.InsertTalepAsync(talep);
            return View();
        }

        [HttpPost, ActionName("AddTeklif")]
        public virtual async Task<IActionResult> TopluTeklifAdd(int topluteklifId, TopluTeklifModel model)
        {
            var teklif = new TopluTeklif
            {
                Price = model.Price,
                TalepId = model.TalepId,
                VendorId = model.VendorId
            };

            await _topluteklifService.InsertTeklifAsync(teklif);
            return View();
        }

        public virtual async Task<IActionResult> TalepDetails(int talepId)
        {
            var talep = _toplutalepService.GetTalepByIdAsync(talepId);
            if (talep == null)
                return Challenge();
            var model = await _toplutalepModelFactory.PrepareTalepModelByIdAsync(talepId);
            return View(model);
        }

        public virtual async Task<IActionResult> TeklifDetails(int teklifId)
        {
            var teklif = _topluteklifService.GetTeklifByIdAsync(teklifId);
            if (teklif == null)
                return Challenge();
            var model = await _topluteklifModelFactory.PrepareTeklifModelByIdAsync(teklifId);
            return View(model);
        }

        public virtual async Task<IActionResult> TalepList()
        {
            var model = await _toplutalepModelFactory.PrepareTopluTalepListModelAsync();
            return View("List", model);
        }

        public virtual async Task<IActionResult> TeklifList()
        {
            var model = await _topluteklifModelFactory.PrepareTopluTeklifListModelAsync();
            return View("List", model);
        }

    }
}
