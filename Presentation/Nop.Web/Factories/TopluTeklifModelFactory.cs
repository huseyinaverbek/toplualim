﻿using Nop.Core;
using Nop.Core.Domain.BulkOrder;
using Nop.Services.BulkOrder;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Topics;
using Nop.Web.Models.BulkOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Factories
{
    public partial class TopluTeklifModelFactory : ITopluTeklifModelFactory
    {
        #region Fields

        private readonly IAclService _aclService;
        private readonly ILocalizationService _localizationService;
        private readonly IStoreContext _storeContext;
        private readonly IStoreMappingService _storeMappingService;
        private readonly ITopluTeklifService _TeklifService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IDateTimeHelper _dateTimeHelper;

        #endregion

        public TopluTeklifModelFactory(IAclService aclService,
        ILocalizationService localizationService,
        IStoreContext storeContext,
        IStoreMappingService storeMappingService,
        ITopluTeklifService TeklifService,
        IUrlRecordService urlRecordService,
        IDateTimeHelper dateTimeHelper)
        {
            _aclService = aclService;
            _localizationService = localizationService;
            _storeContext = storeContext;
            _storeMappingService = storeMappingService;
            _TeklifService = TeklifService;
            _urlRecordService = urlRecordService;
            _dateTimeHelper = dateTimeHelper;
        }



        #region Utilities

        /// <summary>
        /// Prepare the topic model
        /// </summary>
        /// <param name="Teklif">Topic</param>
        /// <returns>
        /// A task that represents the asynchronous operation
        /// The task result contains the opic model
        /// </returns>
        protected virtual TopluTeklifModel PrepareTeklifModelAsync(TopluTeklif Teklif)
        {
            if (Teklif == null)
                throw new ArgumentNullException(nameof(Teklif));

            var model = new TopluTeklifModel
            {
                Id = Teklif.Id,
                Price = Teklif.Price,
                TalepId = Teklif.TalepId,
                VendorId = Teklif.VendorId
            };

            return model;
        }

        #endregion



        public virtual async Task<TopluTeklifModel> PrepareTeklifModelByIdAsync(int TeklifId, bool showHidden = false)
        {
            var Teklif = await _TeklifService.GetTeklifByIdAsync(TeklifId);

            if (Teklif == null)
                return null;

            if (showHidden)
                return PrepareTeklifModelAsync(Teklif);

            return PrepareTeklifModelAsync(Teklif);
        }

        public virtual async Task<TopluTeklifListModel> PrepareTopluTeklifListModelAsync()
        {
            var tekliflist = await _TeklifService.GetAllTeklifsAsync();


            var model = new TopluTeklifListModel
            {
                tekliflist = await tekliflist.SelectAwait(async teklif =>
                {
                    var teklifModel = new TopluTeklifModel();
                    PrepareTeklifModelAsync(teklif);
                    return teklifModel;
                }).ToListAsync()
            };


            return model;
        }
    }

}
