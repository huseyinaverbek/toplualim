﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Models.BulkOrder
{
    public partial record TopluTeklifListModel : BaseNopModel
    {
        public TopluTeklifListModel()
        {
            tekliflist = new List<TopluTeklifModel>();
        }

        public IList<TopluTeklifModel> tekliflist { get; set; }
    }
}
