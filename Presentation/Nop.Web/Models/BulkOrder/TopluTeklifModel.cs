﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Models.BulkOrder
{
    public partial record TopluTeklifModel : BaseNopEntityModel
    {
        public int Price { get; set; }
        public int TalepId { get; set; }
        public int VendorId { get; set; }
    }
}
