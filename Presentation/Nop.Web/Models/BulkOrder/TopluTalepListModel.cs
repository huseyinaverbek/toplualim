﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Models.BulkOrder
{ 
    public partial record TopluTalepListModel : BaseNopModel
{
    public TopluTalepListModel()
    {
        taleplist = new List<TopluTalepModel>();
    }

    public IList<TopluTalepModel> taleplist { get; set; }
}
}