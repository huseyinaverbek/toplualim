﻿using Nop.Core;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Plugins;
using Nop.Web.Framework.Infrastructure;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.Whatsapp
{
    public class WhatsappPlugin : BasePlugin, IWidgetPlugin
    {
        private readonly IWebHelper _webHelper;
        private readonly ISettingService _settingService;

        #region Ctor

        public WhatsappPlugin(IWebHelper webHelper, ISettingService settingService)
        {
            _webHelper = webHelper;
            _settingService = settingService;
        }

        #endregion

        public bool HideInWidgetList => false;

        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "WhatsappWidget";
        }

        public Task<IList<string>> GetWidgetZonesAsync()
        {
            return Task.FromResult<IList<string>>(new List<string> { PublicWidgetZones.HeadHtmlTag });
        }

        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + "Admin/WidgetsWhatsapp/Configure";
        }

        public override async Task InstallAsync()
        {
            // custom logic like adding settings, locale resources and database table(s) here

            await base.InstallAsync();
        }

        public override async Task UninstallAsync()
        {
            // custom logic like removing settings, locale resources and database table(s) which was created during widget installation

            await base.UninstallAsync();
        }
    }
}
