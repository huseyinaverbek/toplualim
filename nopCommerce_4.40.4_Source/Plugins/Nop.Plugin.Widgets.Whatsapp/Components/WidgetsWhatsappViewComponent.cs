﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.Whatsapp.Models;
using Nop.Services.Configuration;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.Whatsapp.Components
{
    [ViewComponent(Name = "WhatsappWidget")]
    public class WidgetsWhatsappViewComponent: NopViewComponent
    {
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;

        public WidgetsWhatsappViewComponent(IStoreContext storeContext,ISettingService settingService)
        {
            _storeContext = storeContext;
            _settingService = settingService;
        }
        public async Task<IViewComponentResult> InvokeAsync(string widgetZone, object additionalData)
        {
            var whatsappSettings = await _settingService.LoadSettingAsync<WhatsappSettings>((await _storeContext.GetCurrentStoreAsync()).Id);

            var model = new ConfigurationModel
            {
                Number = whatsappSettings.Number,
                Message = whatsappSettings.Message
            };

            return View("~/Plugins/Widgets.Whatsapp/Views/PublicInfo.cshtml", model);
        }
    }
}
