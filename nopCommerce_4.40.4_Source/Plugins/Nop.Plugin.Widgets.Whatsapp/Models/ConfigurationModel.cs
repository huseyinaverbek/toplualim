﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;


namespace Nop.Plugin.Widgets.Whatsapp.Models
{
    public class ConfigurationModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Number")]
        public string Number { get; set; }
        public bool Number_OverrideForStore { get; set; }

        [NopResourceDisplayName("Message")]
        public string Message { get; set; }
        public bool Message_OverrideForStore { get;  set; }
    }
}
