﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.Whatsapp.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.Whatsapp.Controllers
{
    [Area(AreaNames.Admin)]
    [AuthorizeAdmin]
    [AutoValidateAntiforgeryToken]
    public class WidgetsWhatsappController: BasePluginController
    {
        #region Fields
        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;

        #endregion

        #region ctor
        public WidgetsWhatsappController(ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService,
            ISettingService settingService,
            IStoreContext storeContext)
        {
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _settingService = settingService;
            _storeContext = storeContext;
        }
        #endregion

        public async Task<IActionResult> Configure()
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var whatsappSettings = await _settingService.LoadSettingAsync<WhatsappSettings>(storeScope);

            var model = new ConfigurationModel
            {
                // configuration model settings here

                Number = whatsappSettings.Number,
                Message = whatsappSettings.Message,
            };

            if (storeScope > 0)
            {
                // override settings here based on store scope

                model.Number_OverrideForStore = await _settingService.SettingExistsAsync(whatsappSettings, x => x.Number, storeScope);
                model.Message_OverrideForStore = await _settingService.SettingExistsAsync(whatsappSettings, x => x.Message, storeScope);
            }

            return View("~/Plugins/Widgets.Whatsapp/Views/Configure.cshtml", model);
        }


        [HttpPost]
        /// <returns>A task that represents the asynchronous operation</returns>
        public async Task<IActionResult> Configure(ConfigurationModel model)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var whatsappSettings = await _settingService.LoadSettingAsync<WhatsappSettings>(storeScope);

            whatsappSettings.Number = model.Number;
            whatsappSettings.Message = model.Message;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            await _settingService.SaveSettingOverridablePerStoreAsync(whatsappSettings, x => x.Number, model.Number_OverrideForStore, storeScope, false);
            await _settingService.SaveSettingOverridablePerStoreAsync(whatsappSettings, x => x.Message, model.Message_OverrideForStore, storeScope, false);

            //now clear settings cache
            await _settingService.ClearCacheAsync();

            _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugins.Saved"));

            return await Configure();
        }
    }
}
