﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Models.BulkOrder
{
    public partial record BulkOrderDetailsModel : BaseNopEntityModel
    {
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public DateTime? BulkOrderStartUtc { get; set; }
        public int BulkOrderStatusId { get; set; }
    }
}
