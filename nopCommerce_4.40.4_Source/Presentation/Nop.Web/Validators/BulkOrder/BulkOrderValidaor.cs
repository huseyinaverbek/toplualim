﻿using FluentValidation;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.BulkOrder;

namespace Nop.Web.Validators.BulkOrder
{
    public partial class BulkOrderValidator: BaseNopValidator<BulkOrderDetailsModel>
    {
        public BulkOrderValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Quantity).GreaterThan(100);
        }
    }
}
