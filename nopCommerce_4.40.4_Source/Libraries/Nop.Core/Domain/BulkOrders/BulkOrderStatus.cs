﻿namespace Nop.Core.Domain.BulkOrder
{
    public enum BulkOrderStatus
    {
        /// <summary>
        /// Pending
        /// </summary>
        Active = 10,

        /// <summary>
        /// Processing
        /// </summary>
        Processing = 20,

        /// <summary>
        /// Complete
        /// </summary>
        Complete = 30,

        /// <summary>
        /// Cancelled
        /// </summary>
        Passive = 40
    }
}
