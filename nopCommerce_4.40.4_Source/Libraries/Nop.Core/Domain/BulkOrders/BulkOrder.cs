﻿using System;

namespace Nop.Core.Domain.BulkOrder
{
    public partial class BulkOrder: BaseEntity
    {
        /// <summary>
        /// Gets or sets the customer identifier
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the product identifier
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the quantity
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the order date and time
        /// </summary>
        public DateTime? BulkOrderStartUtc { get; set; }

        /// <summary>
        /// Gets or sets the order status
        /// </summary>
        public int BulkOrderStatusId { get; set; }

        /// <summary>
        /// Gets or sets the bulk order status
        /// </summary>
        public BulkOrderStatus BulkOrderStatus
        {
            get => (BulkOrderStatus)BulkOrderStatusId;
            set => BulkOrderStatusId = (int)value;
        }
    }
}
