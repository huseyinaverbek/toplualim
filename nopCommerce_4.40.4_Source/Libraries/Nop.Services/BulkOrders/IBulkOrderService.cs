﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core.Domain.BulkOrder;

namespace Nop.Services.BulkOrders
{
    public partial interface IBulkOrderService
    {
        /// <summary>
        /// Deletes a bulk order
        /// </summary>
        /// <param name="bulkOrder">Topic</param>
        /// <returns>A task that represents the asynchronous operation</returns>
        Task DeleteBulkOrderAsync(BulkOrder bulkOrder);

        /// <summary>
        /// Gets a topic
        /// </summary>
        /// <param name="bulkOrderId">The topic identifier</param>
        /// <returns>
        /// A task that represents the asynchronous operation
        /// The task result contains the opic
        /// </returns>
        Task<BulkOrder> GetBulkOrderByIdAsync(int bulkOrderId);

        /// <summary>
        /// Gets all bulk orders
        /// </summary>
        /// <param name="quantity">Store identifier; pass 0 to load all records</param>
        /// <param name="productId">A value indicating whether to ignore ACL rules</param>
        /// <param name="bulkOrderStatusId">A value indicating whether to show hidden topics</param>
        /// <returns>
        /// A task that represents the asynchronous operation
        /// The task result contains the bulk orders
        /// </returns>
        Task<IList<BulkOrder>> GetAllBulkOrdersAsync(int storeId = 0);


        /// <summary>
        /// Inserts a topic
        /// </summary>
        /// <param name="bulkOrder">Topic</param>
        /// <returns>A task that represents the asynchronous operation</returns>
        Task InsertBulkOrderAsync(BulkOrder bulkOrder);

        /// <summary>
        /// Updates the topic
        /// </summary>
        /// <param name="bulkOrder">Topic</param>
        /// <returns>A task that represents the asynchronous operation</returns>
        Task UpdateBulkOrderAsync(BulkOrder bulkOrder);
    }
}
