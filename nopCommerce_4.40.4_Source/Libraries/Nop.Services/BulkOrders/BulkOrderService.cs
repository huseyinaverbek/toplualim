﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.BulkOrder;
using Nop.Data;
using Nop.Services.Customers;
using Nop.Services.Security;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Services.BulkOrders
{
    public partial class BulkOrderService : IBulkOrderService
    {
        #region Fields

        private readonly IAclService _aclService;
        private readonly ICustomerService _customerService;
        private readonly IRepository<BulkOrder> _bulkOrderRepository;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public BulkOrderService(
            IAclService aclService,
            ICustomerService customerService,
            IRepository<BulkOrder> topicRepository,
            IStaticCacheManager staticCacheManager,
            IStoreMappingService storeMappingService,
            IWorkContext workContext)
        {
            _aclService = aclService;
            _customerService = customerService;
            _topicRepository = topicRepository;
            _staticCacheManager = staticCacheManager;
            _storeMappingService = storeMappingService;
            _workContext = workContext;
        }

        #endregion


        public virtual async Task DeleteBulkOrderAsync(BulkOrder bulkOrder)
        {
            await _bulkOrderRepository.DeleteAsync(bulkOrder);
        }

        //public virtual async Task<IPagedList<BulkOrder>> GetAllBulkOrdersAsync(int storeId = 0)
        //{
        //    var bulkOrders = await _bulkOrderRepository.GetAllPagedAsync(async query =>
        //    {
        //        query = query.Where(n => n.BulkOrderStatusId == 10);


        //        //apply store mapping constraints
        //        query = await _storeMappingService.ApplyStoreMapping(query, storeId);

        //        return query.OrderByDescending(n => n.BulkOrderStartUtc);
        //    });

        //    return bulkOrders;
        //}

        public virtual async Task<BulkOrder> GetBulkOrderByIdAsync(int bulkOrderId)
        {
            return await _bulkOrderRepository.GetByIdAsync(bulkOrderId, cache => default);
        }

        public virtual async Task InsertBulkOrderAsync(BulkOrder bulkOrder)
        {
            await _bulkOrderRepository.InsertAsync(bulkOrder);
        }

        public virtual async Task UpdateBulkOrderAsync(BulkOrder bulkOrder)
        {
            await _bulkOrderRepository.UpdateAsync(bulkOrder);
        }

        Task<IList<BulkOrder>> IBulkOrderService.GetAllBulkOrdersAsync(int storeId)
        {
            throw new NotImplementedException();
        }
    }
}
