﻿using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.BulkOrder;
using Nop.Data.Extensions;
using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Builders.BulkOrderBuilder
{
    /// <summary>
    /// Represents a bulk order entity builder
    /// </summary>
    public partial class BulkOrderBuilder : NopEntityBuilder<BulkOrder>
    {
        #region Methods

        /// <summary>
        /// Apply entity configuration
        /// </summary>
        /// <param name="table">Create table expression builder</param>
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(BulkOrder.CustomerId)).AsInt32().ForeignKey<Customer>()
                .WithColumn(nameof(BulkOrder.ProductId)).AsInt32().ForeignKey<Product>();
        }

        #endregion
    }
}