﻿using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.BulkOrder;
using Nop.Core.Domain.Vendors;
using Nop.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Nop.Data.Mapping.Builders.BulkOrder
{
    public partial class TopluTeklifBuilder : NopEntityBuilder<TopluTeklif>
    {
        /// <summary>
        /// Apply entity configuration
        /// </summary>
        /// <param name="table">Create table expression builder</param>
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(TopluTeklif.TalepId)).AsInt32().ForeignKey<TopluTalep>(onDelete: Rule.None)
                .WithColumn(nameof(TopluTeklif.VendorId)).AsInt32().ForeignKey<Vendor>(onDelete: Rule.None);
        }
    }
}
