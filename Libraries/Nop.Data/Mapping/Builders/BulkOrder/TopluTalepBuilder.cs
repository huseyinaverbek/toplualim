﻿using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.BulkOrder;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.Builders.BulkOrder
{
    public partial class TopluTalepBuilder:NopEntityBuilder<TopluTalep>
    {
        /// <summary>
        /// Apply entity configuration
        /// </summary>
        /// <param name="table">Create table expression builder</param>
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(TopluTalep.ProductId)).AsInt32().ForeignKey<Product>(onDelete: Rule.None)
                .WithColumn(nameof(TopluTalep.CustomerId)).AsInt32().ForeignKey<Customer>(onDelete: Rule.None);
        }
    }
}
