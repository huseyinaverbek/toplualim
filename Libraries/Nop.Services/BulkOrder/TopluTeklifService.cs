﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.BulkOrder;
using Nop.Data;
using Nop.Services.Customers;
using Nop.Services.Security;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.BulkOrder
{
    public partial class TopluTeklifService : ITopluTeklifService
    {
        #region Fields

        private readonly IAclService _aclService;
        private readonly ICustomerService _customerService;
        private readonly IRepository<TopluTeklif> _topluTeklifRepository;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public TopluTeklifService(
            IAclService aclService,
            ICustomerService customerService,
            IRepository<TopluTeklif> topluTeklifRepository,
            IStaticCacheManager staticCacheManager,
            IStoreMappingService storeMappingService,
            IWorkContext workContext)
        {
            _aclService = aclService;
            _customerService = customerService;
            _topluTeklifRepository = topluTeklifRepository;
            _staticCacheManager = staticCacheManager;
            _storeMappingService = storeMappingService;
            _workContext = workContext;
        }

        #endregion

        public virtual async Task DeleteTeklifAsync(TopluTeklif topluTeklif)
        {
            await _topluTeklifRepository.DeleteAsync(topluTeklif);
        }

        public virtual async Task<IList<TopluTeklif>> GetAllTeklifsAsync(int talepid = 0)
        {
            return await _topluTeklifRepository.GetAllAsync(query =>
            {
                if(talepid != 0)
                {
                    query = query.Where(t => t.TalepId == talepid);
                }                
                return query.OrderBy(t => t.Id);
            }
            );
        }

        public virtual async Task<TopluTeklif> GetTeklifByIdAsync(int TopluTeklifId)
        {
            return await _topluTeklifRepository.GetByIdAsync(TopluTeklifId, cache => default);
        }

        public virtual async Task InsertTeklifAsync(TopluTeklif topluTeklif)
        {
            await _topluTeklifRepository.InsertAsync(topluTeklif);
        }

        public virtual async Task UpdateTeklifAsync(TopluTeklif topluTeklif)
        {
            await _topluTeklifRepository.UpdateAsync(topluTeklif);
        }
    }
}
