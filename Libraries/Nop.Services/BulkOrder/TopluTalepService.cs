﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.BulkOrder;
using Nop.Data;
using Nop.Services.Customers;
using Nop.Services.Security;
using Nop.Services.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.BulkOrder
{
    public partial class TopluTalepService : ITopluTalepService
    {
        #region Fields

        private readonly IAclService _aclService;
        private readonly ICustomerService _customerService;
        private readonly IRepository<TopluTalep> _toplutalepRepository;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IWorkContext _workContext;

        #endregion

        #region Ctor

        public TopluTalepService(
            IAclService aclService,
            ICustomerService customerService,
            IRepository<TopluTalep> toplutalepRepository,
            IStaticCacheManager staticCacheManager,
            IStoreMappingService storeMappingService,
            IWorkContext workContext)
        {
            _aclService = aclService;
            _customerService = customerService;
            _toplutalepRepository = toplutalepRepository;
            _staticCacheManager = staticCacheManager;
            _storeMappingService = storeMappingService;
            _workContext = workContext;
        }

        #endregion

        public virtual async Task DeleteTalepAsync(TopluTalep toplutalep)
        {
            await _toplutalepRepository.DeleteAsync(toplutalep);
        }

        public virtual async Task<IList<TopluTalep>> GetAllTalepsAsync()
        {
            return await _toplutalepRepository.GetAllAsync(query =>
            {
                return query.OrderBy(t => t.BulkOrderCreatedOnUtc);
            }
            );
        }


        public virtual async Task<TopluTalep> GetTalepByIdAsync(int TopluTalepId)
        {
            return await _toplutalepRepository.GetByIdAsync(TopluTalepId, cache => default);
        }

        public virtual async Task InsertTalepAsync(TopluTalep toplutalep)
        {
            await _toplutalepRepository.InsertAsync(toplutalep);
        }

        public virtual async Task UpdateTalepAsync(TopluTalep toplutalep)
        {
            await _toplutalepRepository.UpdateAsync(toplutalep);
        }
    }
}
