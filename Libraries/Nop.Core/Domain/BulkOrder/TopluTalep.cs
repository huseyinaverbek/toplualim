﻿using Nop.Core.Domain.Common;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Seo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.BulkOrder
{
    public partial class TopluTalep : BaseEntity, ILocalizedEntity, ISlugSupported
    {
        public Guid BulkOrderId { get; set; }
        
        public int Quantity { get; set; }
        public int ProductId { get; set; }
        public int CustomerId { get; set; }
        public int BulkOrderStatusId { get; set; }

        public DateTime BulkOrderCreatedOnUtc { get; set; }


        public BulkOrderStatus BulkOrderStatus
        {
            get => (BulkOrderStatus)BulkOrderStatusId;
            set => BulkOrderStatusId = (int)value;
        }

    }
}
