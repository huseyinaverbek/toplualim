﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.BulkOrder
{
    public partial class TopluTeklif : BaseEntity
    {
        public Guid TeklifId { get; set; }

        public int Price { get; set; }
        public int TalepId { get; set; }
        public int VendorId { get; set; }


    }
}
